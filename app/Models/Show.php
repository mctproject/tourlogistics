<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
    protected $table = 'shows';
    protected $fillable = ['abbreviation', 'show_title'];
}
