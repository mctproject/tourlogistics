<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function presenter()
    {
        return $this->belongsTo(Presenter::class,'id', 'presenter_id');
    }
    public function tour_team()
    {
        return $this->belongsTo(tourTeam::class,'id', 'tour_team_id');
    }
    
    public static function getBookings($week_id,$tour_team_id)
    {
        return Booking::where('week_id',$week_id)->where('tour_team_id',$tour_team_id)->get();
    }
    public static function getBooking($week_id,$tour_team_id)
    {
        return Booking::where('week_id',$week_id)->where('tour_team_id',$tour_team_id)->take(1);
    }
    public static function getBookingTile($week_id,$tour_team_id)
    {
        return \DB::table('bookings')
        ->select(array('presenter_contacts.presenter_id','bookings.id as booking_id','presenters.presenter_name',
        'bookings.week_id','weeks.start_date','bookings.tour_team_id','bookings.booking_status_id',
        'booking_statuses.name as booking_status_name','booking_statuses.description as booking_status_description',
         'bookings.routing_issues','contacts.city','states.abbreviation','regions.color'))
        ->join('presenter_contacts','presenter_contacts.presenter_id','=','bookings.presenter_id')
        ->join('presenters','presenters.id','=','presenter_contacts.presenter_id')
        ->join('contacts','contacts.id','=','presenter_contacts.contact_id')
        ->join('states','states.id','=','contacts.state_id')
        ->join('regions','regions.id','=','states.region_id')
        ->join('weeks','bookings.week_id','=','weeks.id')
        ->join('booking_statuses','booking_statuses.id','=','bookings.booking_status_id')
        ->where('presenter_contacts.contact_type_id', Contact_type::getPrimaryContactId())
        ->where('bookings.week_id',$week_id)
        ->where('bookings.tour_team_id',$tour_team_id)
        ->get();
    }
}
