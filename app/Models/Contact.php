<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $fillable = ['contact_name', 'email', 'address', 'city', 'zip_code', 'state_id', 'contact_type_id'];

    public function presenters()
    {

        return $this->belongsToMany(Presenter::class, 'presenter_contacts')->withPivot('contact_type_id');

    }

    public function state()
    {
        return $this->belongsTo(State::class);

    }
}
