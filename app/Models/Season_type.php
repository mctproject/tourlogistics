<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Season_type extends Model
{
    //
    public static function GetName($id)
    {
        $type = Season_type::find($id)->first();
        //could check for if there is a type but we want to know if there is a problem.
        return $type->name;
    }
}
