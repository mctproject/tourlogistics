<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tour_team_week extends Model
{
    //
    public function weeks(){

        return $this->belongsToMany(Week::class, 'season_weeks');

    }
}
