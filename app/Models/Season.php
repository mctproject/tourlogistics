<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Season extends Model
{
    protected $table = 'seasons';
    protected $fillable = ['complete','type','name'];

    public function weeks(){

        return $this->belongsToMany(Week::class, 'season_weeks')->orderby('start_date');

    }
    public static function topThree()
    {
        //TODO: need to sort by date you will have to look at the weeks that are connected to this season
        return Season::all()->sortByDesc('id')->take(3);
    }   
    public function tour_teams()
    {
        return $this->hasMany(Tour_team::class,'season_id')->orderby('show_id');
    }
}
