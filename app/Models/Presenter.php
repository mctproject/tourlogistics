<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presenter extends Model
{
    protected $table = 'presenters';
    protected $fillable = ['presenter_name','is_fly_school'];

    public function contacts(){

        return $this->belongsToMany(Contact::class, 'presenter_contacts')->withPivot('contact_type_id');

    }
    public function primary_contact(){

        return $this->belongsToMany(Contact::class, 'presenter_contacts')->withPivot('contact_type_id')->where('contact_type_id', Contact_type::getPrimaryContactId());

    }
    public function res_requests(){

        return $this->belongsToMany(Res_requests::class, 'res_requests' );

    }
    // public function location()
    // {
    //     return   $this->belongsToMany(Contact::class, 'presenter_contacts')->withPivot('contact_type_id')->where('contact_type_id', Contact_type::getPrimaryContactId())->first();
    //     //dd($contact);
    //     return $contact->city . ", ". $contact->state_id;
    // }
    public function presenter_color()
    {
        return $this->belongsToMany(Contact::class, 'presenter_contacts')->withPivot('contact_type_id')->where('contact_type_id', Contact_type::getPrimaryContactId())->first()->state()->region();
        // return \DB::table('presenter_contacts')
        // ->select('color')
        // ->join('contacts','contacts.id','=','presenter_contacts.contact_id')
        // ->join('states','states.id','=','contacts.id')
        // ->join('regions','regions.id','=','states.region_id')
        // ->where('presenter_contacts.contact_type_id', Contact_type::getPrimaryContactId())
        // ->where('presenter_contacts.presenter_id',$pesenter_id)
        // ->get();
        //return Presenter::find($pesenter_id)->primary_contact->state->region->region_color;
    }

    public static function PastShows($presenter_id,$season_id)
    {
        $date = Season::where('id',$season_id)->get();
        $date = ($date->first()->weeks->sortBy('start_date')->first()->start_date);
        $result = \DB::select(\DB::raw("select GROUP_CONCAT(x.showName order by x.end_date desc) as past_shows
        from 
        (
        select b.presenter_id,CONCAT(tt.show_id ,' ',tt.show_num) as showName, w.end_date
        from bookings b 
        join tour_teams tt on tt.id = b.tour_team_id
        join weeks w on w.id = b.week_id
        where b.presenter_id = :presenterId
        and w.end_date <= :date
        union 
        select :presenterIdFixed as presenter_id, null as showName, ''as end_date 
        )x
        group by x.presenter_id"), array(
            'presenterId' => $presenter_id,
            'date' => $date,
            'presenterIdFixed' => $presenter_id,
          ));
        return $result;
    }
}
