<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Res_request extends Model
{
    //
    public function availableWeeks(){

        return $this->belongsToMany(Week::class, 'availabilities');

    }

    public function presenter() {

        return $this->belongsTo(Presenter::class);

    }

    public function season() {

        return $this->belongsTo(Season::class);
    }
    private static function getArrayOfApplicableRequests($seasonId, $weekId, $tourTeamId)
    {
        $result = \DB::select(\DB::raw("select rr.id 
        from res_requests rr
        join availabilities a on a.res_request_id = rr.id
        where rr.season_id = :seasonId
        and a.week_id = :weekId
        group by rr.id"), array(
            'seasonId' => $seasonId,
            'weekId' => $weekId,
            //'tourTeamId' => $tourTeamId,
        ));
        // $result = \DB::select(\DB::raw("select abbreviation 
        // from shows"));
        $collect = collect();
        foreach($result as $r)
        {
            $collect->push($r->id);
        }
        return $collect->toArray();
    }
    public static function getApplicableRequests($seasonId, $weekId, $tourTeamId)
    {
        $result = Res_request::getArrayOfApplicableRequests($seasonId, $weekId, $tourTeamId);
        if(count($result)>0)
        {
            $requests = Res_request::whereIn('id',$result)->get();
        }
        else
        {
            $requests = null;
        }
        return $requests;
    }
    public static function getOtherRequests($seasonId, $weekId, $tourTeamId)
    {
        $result = Res_request::getArrayOfApplicableRequests($seasonId, $weekId, $tourTeamId);
        if(count($result)>0)
        {
            $requests = Res_request::whereNotIn('id',$result)->where('season_id',$seasonId)->get();
        }
        else
        {
            $requests = Res_request::where('season_id',$seasonId)->get();
        }
        //dd($requests);
        return $requests;
    }
}
