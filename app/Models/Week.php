<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Week extends Model
{
    protected $dates = ['start_date','end_date'];
    public static function display($week)
    { 
        return $week->start_date->format('M, d')." - ".$week->end_date->format('M, d');
    }
    public static function displayDates($start_date,$end_date)
    {
        return (new \DateTime($start_date))->format('M, d')." - ".(new \DateTime($end_date))->format('M, d');
    }
    public static function GetRangeWeeks($startDate,$endDate)
    {
        return Week::whereBetween('start_date',array($startDate,$endDate))->get();//->sortBy('start_date')
    }
}
