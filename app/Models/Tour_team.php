<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tour_team extends Model
{
    protected $table = 'tour_teams';
    protected $fillable = ['complete','show_id','season_id'];

    public function weeks(){

        return $this->belongsToMany('App\Week','season_weeks');

    }
    public static function display($tour_team)
    {
        return $tour_team->show_id." ".$tour_team->show_num;
    }
    public function show(){
        return $this->hasOne(Show::class, 'abbreviation', 'show_id');
    }
    public function season(){
        return $this->hasOne(Season::class, 'id', 'season_id');
    }
    public static function getShowsBySeason($id){
        $data = Tour_team::where('season_id','=',$id)->get();
        return $data;
    }
    public function bookings()
    {
        return $this->hasMany(Booking::class,'tour_team_id');
    }
    public static function getNumShowsInSeason($id1,$id2){
        $count = Tour_team::where('season_id', '=', $id1)->where("show_id", '=', $id2)->count();
        return $count+1;
    }
}
