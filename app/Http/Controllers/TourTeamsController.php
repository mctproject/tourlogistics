<?php

namespace App\Http\Controllers;

use App\Models\Tour_team;
use Illuminate\Http\Request;

class TourTeamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $data['tour_teams'] = \App\Models\Tour_team::all();
        $data['contacts'] = \App\Models\Contact::all();
        $data['seasons'] = \App\Models\Season::all();
        $data['shows'] = \App\Models\Show::all();
        $data['title'] = "All Tour Teams";

        return view('tourTeams', $data);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['seasons'] = \App\Models\Season::all();
        $data['shows'] = \App\Models\Show::all();

        return view('tourTeams/create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tour_team = new Tour_team();
        $tour_team->season_id = $request->input('season');
        $tour_team->show_id = $request->input('show');
        $tour_team->complete = $request->input('complete');

        // Get the current show number for that season.
        $tour_team->show_num = \App\Models\Tour_team::getNumShowsInSeason($tour_team->season_id,$tour_team->show_id);
        $tour_team->save();

        return redirect()->route('tourTeams.show', $tour_team->season_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['tour_teams'] = Tour_team::getShowsBySeason($id);
        $data['contacts'] = \App\Models\Contact::all();
        $data['seasons'] = \App\Models\Season::all();
        $data['shows'] = \App\Models\Show::all();
        $collection = \App\Models\Season::where('id', "=", $id)->pluck('name');
        $data['title'] = $collection[0];


        return view('tourTeams', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $team_to_delete = Tour_team::where('id', "=", $id)->first();

        $temp = $team_to_delete; // Temp so I can delete.
        $team_to_delete->delete();
        $show_id = $temp->show_id;
        $season_id = $temp->season_id;

        $data = Tour_team::getShowsBySeason($season_id);

        foreach($data as $show){

            if($show->show_id == $show_id && $show->show_num > 1){
                $show->show_num = $show->show_num-1;
                $show->save();
            }
        }

        return redirect()->route('tourTeams.show', $season_id);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tour_id = $request->input('tour-id');

        $tour_team = Tour_team::where('id', "=", $tour_id)->first();

        // Get all old data
        $season_id = $tour_team->season_id;
        $new_show = $request->input('show-select');
        $old_show = $tour_team->show_id;
        $old_num = $tour_team->show_num;

        $data = Tour_team::getShowsBySeason($season_id);

        if($old_show != $new_show){

            $tour_team->show_id = $new_show;
            $tour_team->show_num = \App\Models\Tour_team::getNumShowsInSeason($season_id, $new_show);
            $tour_team->save(); // Save the tour team with the new show num

            $data = Tour_team::getShowsBySeason($season_id);

            foreach($data as $show){

                if($show->show_id == $old_show && $show->show_num > $old_num){
                    $show->show_num = $show->show_num -1;

                    $show->save(); // Save the tour_team with the new show num if changed
                }


            }

        }

        return redirect()->route('tourTeams.show', $season_id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {


    }

}
