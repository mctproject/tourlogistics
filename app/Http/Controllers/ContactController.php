<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Contact_type;
use App\Models\Presenter;
use App\Models\State;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['presenters'] = Presenter::all();
        $data['states'] = State::all();
        $data['contactTypes'] = Contact_type::all();
        return view('presenters/createContact', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $presenter = Presenter::find($request->input('presenter-name'));
        $contact = new Contact();
        $contact->contact_name = $request->input('contact-name');
        $contact->email = $request->input('email');
        $contact->address = $request->input('address');
        $contact->city = $request->input('city');
        $contact->zip_code = $request->input('zip-code');
        $contact->state_id = $request->input('state');
        $contact->save();
        $presenter->contacts()->save($contact,['contact_type_id' => $request->input('type')]);

        return redirect('presenters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
