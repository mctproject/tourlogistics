<?php

namespace App\Http\Controllers;

use App\Models\Presenter;
use App\Models\Res_request;
use App\Models\Season;
use Illuminate\Http\Request;

class ResRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['requests'] = Res_request::all();

        return view('requests', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //
        $resRequest = new Res_request();
        $resRequest->season_id = $request->input('season');
        $resRequest->presenter_id = $request->input('presenter');
        $resRequest->num_wks_req = $request->input('number-weeks');
        $resRequest->consecutive_wks   = $request->input('consecutive-weeks');
        $resRequest->save();


        foreach($request->input('weeks') as $week){

            $resRequest->availableWeeks()->attach($week);

        }
        return redirect('requests');
        //return redirect('requests/'.$resRequest->id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Res_request $request)
    {

        //$request = Res_request::find($id);
        //echo "We're Editing!!";
        //dump($request);




    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSeasonWeeks(Request $request) {

        $resRequest = new Res_request();
        $resRequest->season_id = $request->input('season');
        $resRequest->presenter_id = $request->input('presenter');
        $resRequest->num_wks_req = $request->input('number-weeks');
        $resRequest->consecutive_wks   = $request->input('consecutive-weeks');

        $data['request'] = $resRequest;
        $data['season'] = Season::find($resRequest->season_id);
        $data['presenter'] = Presenter::find($resRequest->presenter_id);

        return view('selectResRequestWeeks', $data);

    }

}
