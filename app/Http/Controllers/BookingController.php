<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Res_request;
use App\Models\Season;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createBooking($seasonId, $weekId=null, $tourTeamId=null)
    {
        if($weekId == null || $tourTeamId == null)
        {
            $data['didSelectedTile'] = false;
            $data['requests'] =Res_request::where('season_id',$seasonId)->get();
            $data['otherRequests'] = null;
        }
        else
        {
            $data['didSelectedTile'] = true;
            $data['requests'] = Res_request::getApplicableRequests($seasonId, $weekId, $tourTeamId);
            $data['otherRequests'] = Res_request::getOtherRequests($seasonId, $weekId, $tourTeamId);
        }
        $data['season'] = Season::find($seasonId);
        $data['bookingStatuses'] = \App\Models\Booking_status::all();
        $data['weekId'] = $weekId;
        $data['tourTeamId'] = $tourTeamId;
        return view('addBooking',$data);
    }
    
    
    public function deleteBooking($id)
    {
        $bookingToDelete = Booking::where('id', $id)->first();
        $seasonId = \App\Models\Tour_team::where('id',$bookingToDelete->tour_team_id)->first()->season_id;
        
        $bookingToDelete -> delete();
        return redirect()->route('season.show',$seasonId);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $resRequest = Res_request::where('id',$request->input('hfRequestId'))->first();

        $booking = new Booking();
        $booking->routing_issues = "";
        $booking->week_id = $request->input('bookingWeek');
        $booking->presenter_id = $resRequest->presenter_id;
        $booking->tour_team_id =$request->input('bookingTourTeam');
        $booking->booking_status_id =$request->input('bookingStatus');
        $booking->save();

        return redirect()->route('season.show',$resRequest->season_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function moveBooking(Request $request)
    {
        //dd($request->input('hfSeasonId'));
        $rawData = json_decode($request->input('hfEditBooking'));
        $bookingToUpdate = Booking::find($rawData->booking_id);
        $bookingToUpdate->week_id = $request->input('bookingWeek');
        $bookingToUpdate->tour_team_id = $request->input('bookingTourTeam');
        $bookingToUpdate->booking_status_id = $request->input('bookingStatus');
        $bookingToUpdate->save();

        return redirect()->route('season.show',$request->input('hfSeasonId'));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
