<?php

namespace App\Http\Controllers;

use App\Models\Season;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function test($id) {
        $this->data['season'] = Season::Find($id);
        //dd($this->data);
        return view('season',$this->data);
    }

    public function index() {
        $this->data['seasons'] = Season::all();
        //dd($this->data);
        return view('season',$this->data);
    }
}
