<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Presenter;
use App\Models\Presenter_contact;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PresenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (request()->filled('presenter-name')) {
            $data['presenters'] = Presenter::where('presenter_name', 'like',"%".request()->get('presenter-name')."%" )->get();

        } else {
            $data['presenters'] = Presenter::all();
        }

        return view('presenters', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['states'] = State::all();

        return view('presenters/create', $data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = new Contact();
        $contact->contact_name = $request->input('contact-name');
        $contact->email = $request->input('email');
        $contact->address = $request->input('address');
        $contact->city = $request->input('city');
        $contact->zip_code = $request->input('zip-code');
        $contact->state_id = $request->input('state');
        $contact->save();

        $presenter = new Presenter();
        $presenter->presenter_name = $request->input('presenter-name');
        $presenter->is_fly_school = $request->input('fly-school');
        $presenter->save();


        $presenter->primary_contact()->attach($contact->id);
        //$presenter->primary_contact()->save($contact);

        return redirect('presenters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
