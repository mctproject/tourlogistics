<?php

namespace App\Http\Controllers;

use App\Models\Season;
use App\Models\Season_type;
use App\Models\Booking;
use Illuminate\Http\Request;

class SeasonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['seasons'] = Season::all()->sortByDesc("id");
        $data['seasonTypes'] = Season_type::all();

        return view('selectSeason', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $seasonWeeks = \App\Models\Week::GetRangeWeeks($request->input('seasonStartDate'),$request->input('seasonEndDate'));

        $seasonYearText = $seasonWeeks->first()->start_date->year;
        if($seasonYearText != $seasonWeeks->last()->end_date->year )
        {
            $seasonYearText = $seasonYearText . " - " . $seasonWeeks->last()->end_date->year;
        }
        $season = new Season();
        $season->name = \App\Models\Season_type::GetName($request->input('seasonType'))." ".$seasonYearText." (".$request->input('seasonDescription').")";
        $season->complete = false;
        $season->season_type_id = $request->input('seasonType');
        $season->save();

        foreach($seasonWeeks as $week){

            $season->weeks()->attach($week->id);
        }
        return redirect()->route('season.show',$season->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $season= Season::find($id);
        $collection = collect();
        foreach($season->weeks as $week)
        {
            $row = collect();
            $row->push($week);
            foreach($season->tour_teams as $tour_team)
            {
                $row->push(Booking::getBookingTile($week->id,$tour_team->id));
            }
            $collection->push($row);
        }


        $data['season'] = $season;
        $data['bookingStatuses'] = \App\Models\Booking_status::all();
        $data['seasonTour'] = json_decode($collection,true);
        return view('season', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
