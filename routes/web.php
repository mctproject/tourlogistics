<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});


Auth::routes();

Route::post('logout','Auth\LoginController@logout')->name('logout');

Route::middleware('auth')->group(function(){


    Route::resource('requests', 'ResRequestController');
    Route::view('addRequest','requests/addRequest')->name('addRequest');
    Route::resource('season','SeasonController');
    Route::get('home', 'PresenterController@index')->name('home');
    Route::post('requests-add', 'ResRequestController@getSeasonWeeks')->name('getSeasonWeeks');
    Route::resource('tourTeams', 'TourTeamsController');
    Route::delete('tourTeams', 'TourTeamsController@delete');
    Route::resource('presenters', 'PresenterController');
    Route::resource('contacts', 'ContactController');
    Route::resource('shows','ShowController');
    Route::resource('bookings','BookingController');
    // route::route('bookings','BookingController');
    Route::post('moveBooking', 'BookingController@moveBooking')->name('moveBooking');
    Route::get('createBooking/{seasonId}/{weekId?}/{tourTeamId?}', [
        'as' => 'createBooking', 'uses' => 'BookingController@createBooking']);
    Route::get('deleteBooking/{id}', [
            'as' => 'deleteBooking', 'uses' => 'BookingController@deleteBooking']);
    Route::view('admin','admin')->name('admin');
    Route::view('edit_request','edit_request')->name('edit_request');

});

Route::get('test','Controller@index');
