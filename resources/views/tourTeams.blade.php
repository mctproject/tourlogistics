@extends('layouts.master')

@section('title')
    Tours Page
@endsection

@section('head')

@endsection
@section('content')
    <!--
    <div class="row">
        <div class="form-group mb-2">
            <label class="sr-only" for="season-select">Season Select:</label>
            <select name="season-select" id="season-select" class="form-control"
                    onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                <option value="">Season Select</option>
                @foreach ($seasons as $season)
                    <option value="{{route('tourTeams.show', [$season->id])}}">{{$season->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    !-->

    <center><label><font size="24">{{$title}}</font></label></center>

    <div class="row">
        <table class="table table-condensed table-striped table-bordered" id="tours-table" style="border-width: thick">
            <thead>
            <tr>

                <th>Edit Team</th>
                <th>Show Title</th>
                <th>Season</th>
                <th>Remove Team</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($tour_teams as $tour_team)

                <tr>


                    <td>
                        <button class="btn btn-primary" data-toggle="modal" onclick="populateTourTeamId({{$tour_team->id}});" data-target="#editTour">{{$tour_team->show_id}} {{$tour_team->show_num}}</button>
                    </td>

                    <td>{{$tour_team->show->show_title}}</td>
                    <td>{{$tour_team->season->name}}</td>
                    <td>
                        <button class="btn btn-primary"><a style="color:white" href={{route('tourTeams.edit', $tour_team->id)}} onclick="return confirm('Delete {{$tour_team->show->show_title}} from the season {{$tour_team->season->name}}?')">Delete</a> </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <!--Modal for editing tour -->

        <div class="modal fade" id="editTour" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Edit Tour Team</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">



                        <form class="form" id="modal-form" method="post" action="{{route('tourTeams.update', 0)}}">
                            {{ csrf_field() }}
                            {{method_field('PATCH')}}
                            <div class="form-group mb-2">
                                <label for="show-name">New Tour Team Show</label>
                                <select name="show-select" id="show-select" class="form-control">
                                    <option value="none" disabled>Show Select</option>
                                    @foreach ($shows as $show)
                                        <option value='{{$show->abbreviation}}'>
                                            {{$show->show_title}}
                                        </option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="tour-id" id="tour-id" class="form-control-plaintext">
                            </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">OK</button>
                    </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>


@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('table').DataTable({});
        });

        function populateTourTeamId(id){
            document.getElementById("tour-id").value = id;

        }

    </script>
@endsection