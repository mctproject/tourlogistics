@extends('layouts.master')

@section('title')
    Request Page
@endsection

@section('head')
    <?php $seasons = \App\Models\Season::all(); $presenters = \App\Models\Presenter::all(); ?>
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="{{route('getSeasonWeeks')}}">
                    {{ csrf_field() }}
                    <h2>New Request</h2>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="season">Season:</label>
                        <div class="col-sm-9">
                            <select name="season" id="season" class="form-control" aria-controls="help-pickup">
                                <option value="none">Select Season</option>
                                @foreach ($seasons as $season)
                                    <option value='{{$season->id}}'>
                                        {{$season->name}}
                                    </option>
                                @endforeach
                            </select>
                            <p id="help-pickup" class="help-block" aria-live="polite"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="presenter">Presenter:</label>
                        <div class="col-sm-9">
                            <select name="presenter" id="presenter" class="form-control" aria-controls="help-pickup">
                                <option value="none">Select Presenter</option>
                                @foreach ($presenters as $presenter)
                                    <option value='{{$presenter->id}}'>
                                        {{$presenter->presenter_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class=form-group>
                        <label class="col-sm-3 control-label" for="number-weeks">Number of Weeks:</label>
                        <div class="col-sm-9">
                            <input type="number" name="number-weeks" id="number-weeks"
                                   placeholder="Enter Number of Weeks"
                                   class="input form-control"
                                   data-format="1">
                        </div>
                    </div>

                    <div class="form-group" style="">
                        <label class="col-sm-3 control-label" for="consecutive-weeks">Consecutive Weeks:</label>
                        <div class="col-sm-9">
                            <select name="consecutive-weeks" id="consecutive-weeks" class="form-control" aria-controls="help-pickup">
                                <option value="none">Is Consecutive?</option>
                                <option value='1'>
                                    Yes
                                </option>
                                <option value='0'>
                                    No
                                </option>
                            </select>
                            <p id="help-pickup" class="help-block" aria-live="polite"></p>
                        </div>
                    </div>

                    <div class=form-group>
                        <button type="reset" role="button" class="col-sm-2 button btn btn-primary">Cancel</button>
                        <button type="submit" role="button" class="col-sm-2 button btn btn-primary pull-right">Next
                        </button>
                    </div>
                </form>
            </div>
        </div>

@endsection

@section('scripts')

@endsection