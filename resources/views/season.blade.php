@extends('layouts.master')

@section('title')
    Season Page
@endsection
@section('assets')
    
<style>
        #seasonTable.season-table td 
        {
            padding:0px;
            vertical-align: middle;
            width:300px;
            height:80px;
            /* white-space: nowrap; */
        }
        button.season-table 
        {
            width:300px;
            height:80px;
            color:black;
            position: relative;
        }
        a.season-table 
        {
            width:300px;
            height:80px;
            color:white;
            position: relative;
            line-height: 65px;
            background-color: #404040
        }

    </style>
@endsection

@section('content')
    <div class="section-header">
        <div class="btn-toolbar pull-right btn-group">
            <a class="btn btn-primary" href="{{route('createBooking',['seasonId'=>$season->id,'weekId'=>null,'tourTeamId'=>null])}}" > Add Booking</a>
        </div>
        <h3>{{$season->name}}</h3>
    </div>

    <div class="row">
        <table class="table table-condensed table-striped table-bordered season-table" id="seasonTable" style="border-width: thick">
            <thead>
                <tr>
                    <th >Week</th>
                    @foreach($season->tour_teams as $tour_team)
                <th >{{\App\Models\Tour_team::display($tour_team)}}</th>
                    @endforeach
                </tr>
            </thead>
            @foreach($seasonTour as $weekRow)
                {{-- <tr><td>{{$weekRow[0]->week_id}}</td></tr> --}}
                <tr>
                    @foreach($weekRow as $bookings)
                    @if($loop->index==0)
                    <td >
                        {{-- {{dd($bookings)}} , {{$bookings['id']}}--}}
                        {{\App\Models\Week::displayDates($bookings['start_date'],$bookings['end_date'])}}
                    </td>
                    @else
                    <td >
                        @if(count($bookings)==0){{-- Need to add a request for this day. --}}
                        <a href="{{route('createBooking',['seasonId'=>$season->id,'weekId'=>$season->weeks[$loop->parent->index],'tourTeamId'=>$season->tour_teams[$loop->index-1]])}}" class="btn btn-default season-table"> Add Booking</a>
                        @elseif(count($bookings)>1){{-- Multi bookings for a week and tour team. --}}
                        <button class="btn btn-default season-table" type="button" role="button" data-toggle="modal" data-target="#multi_{{$season->weeks[$loop->parent->index]["id"]."_".$season->tour_teams[$loop->index-1]["id"]}}">Multiple Bookings!!!!</button>
                        
                        <div id="multi_{{$season->weeks[$loop->parent->index]["id"]."_".$season->tour_teams[$loop->index-1]["id"]}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h3 class="modal-title" id="exampleModalLabel">Select booking to edit.</h3>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <div class="modal-body">
                                        @foreach($bookings as $booking)
                                        <button class="season-table" onclick="editBookingData({{json_encode($booking)}})" type="button" role="button" data-toggle="modal" data-target="#editBooking" data-dismiss="modal"  style="background-color:#{{$booking['color']}}">
                                            <div style="font-size:.7em;position: absolute;right:25px;top:5px;">
                                                {{$booking['booking_status_name']}}
                                            </div>
                                            <div style="font-size: 1.6em;font-weight: bold;">
                                                {{$booking['city']}}, {{$booking['abbreviation']}}
                                            </div>
                                            <div style="font-size: .7em;">
                                                {{$booking['presenter_name']}}
                                            </div>
                                            <div style="font-size: .7em;float:left">
                                                {{\App\Models\Presenter::PastShows($booking['presenter_id'],$season->id)[0]->past_shows}}
                                            </div>
                                        </button>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        @else {{-- display booking as a card. --}}
                            <button class="season-table" onclick="editBookingData({{json_encode($bookings[0])}})" type="button" role="button" data-toggle="modal" data-target="#editBooking" style="background-color:#{{$bookings[0]['color']}}">
                                <div style="font-size:.7em;position: absolute;right:25px;top:5px;">
                                    {{$bookings[0]['booking_status_name']}}
                                </div>
                                <div style="font-size: 1.6em;font-weight: bold;">
                                    {{$bookings[0]['city']}}, {{$bookings[0]['abbreviation']}}
                                </div>
                                <div style="font-size: .7em;">
                                    {{$bookings[0]['presenter_name']}}
                                </div>
                                <div style="font-size: .7em;float:left">
                                    {{\App\Models\Presenter::PastShows($bookings[0]['presenter_id'],$season->id)[0]->past_shows}}
                                </div>
                            </button>
                        @endif
                    </td>
                    @endif
                    @endforeach
                </tr>
            @endforeach
        </table>
    </div>
    
    <div id="editBooking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Edit Booking</h3>
                        <h4 id="lblEditBookingLocation" ></h4>
                        <h4 id="lblEditBookingPresenter"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="form" id="modal-form" method="post" action="{{route('moveBooking')}}">
                            {{ csrf_field() }}
                            <div class="form-group mb-2">
                                <label class="col-sm-3 control-label" for="bookingWeek">Week:</label>
                                <select name="bookingWeek" id="bookingWeek" class="form-control"
                                        aria-controls="help-pickup">
                                    @foreach ($season->weeks as $week)
                                        <option value='{{$week->id}}'>
                                                {{\App\Models\Week::displayDates($week->start_date,$week->end_date)}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group mb-2">
                                    <label class="col-sm-3 control-label" for="bookingTourTeam">Show:</label>
                                    <select name="bookingTourTeam" id="bookingTourTeam" class="form-control"
                                            aria-controls="help-pickup">
                                        @foreach ($season->tour_teams as $tourTeam)
                                            <option value='{{$tourTeam->id}}'>
                                                    {{\App\Models\Tour_team::display($tourTeam)}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group mb-2">
                                        <label class="col-sm-3 control-label" for="bookingStatus">Status:</label>
                                        <select name="bookingStatus" id="bookingStatus" class="form-control"
                                                aria-controls="help-pickup">
                                            @foreach ($bookingStatuses as $bookingStatus)
                                                <option value='{{$bookingStatus->id}}' title="{{$bookingStatus->description}}">
                                                        {{$bookingStatus->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                
                            <input type="hidden" class="form-control-plaintext" name="hfEditBooking" id="hfEditBooking" value="">
                            <input type="hidden" class="form-control-plaintext" name="hfSeasonId" id="hfSeasonId" value="{{$season->id}}">
                            <div class="modal-footer">
                                <input type="hidden" name="hfDeleteRoute" id="hfDeleteRoute" value="{{route('deleteBooking',['id'=>null])}}">
                                <a id="lnkDeleteBooking" href="{{route('deleteBooking',['id'=>null])}}" class="btn btn-default left" style="float:left;"> Delete Booking</a>
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="submit" role="button" class="btn btn-primary" >Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

{{--@foreach($seasons as $season)
    {{$season->id}}<br/>
    {{$season->weeks}}
    @endforeach--}}

@endsection

@section('scripts')
<script>
    $(document).ready(function () {
        $('#seasonTable').DataTable({
            "scrollY": 600,
            "scrollX": true,
            "searching":false,
            "ordering":false,
            "paging":false,
            "info":false
        });
        $('#seasonTable').DataTable().draw();
    });
    function editBookingData($jsonData)
    {
        $('#bookingWeek').val($jsonData['week_id']);
        $('#bookingTourTeam').val($jsonData['tour_team_id']);
        $('#bookingStatus').val($jsonData['booking_status_id']);
        $('#lblEditBookingLocation').text($jsonData['city']+', '+$jsonData['abbreviation']);
        $('#lblEditBookingPresenter').text($jsonData['presenter_name']);
        $('#hfEditBooking').val(JSON.stringify($jsonData));
        $('#lnkDeleteBooking').attr('href',$('#hfDeleteRoute').val()+"/"+$jsonData['booking_id']);
    }
</script>
@endsection