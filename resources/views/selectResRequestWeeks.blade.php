@extends('layouts.master')

@section('title')
    Edit Request Page
@endsection

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12">

                <h2>Available Weeks</h2>

                <div class="form-group">
                    <div class="col-sm-3 control-label" for="season">Season:</div>
                    <div class="col-sm-9">
                        {{$season->name}}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 control-label" for="presenter">Presenter:</div>
                    <div class="col-sm-9">
                        {{  $presenter->presenter_name}}
                    </div>
                </div>

                <div class=form-group>
                    <div class="col-sm-3 control-label" for="number-weeks">Number of Weeks:</div>
                    <div class="col-sm-9">
                        {{$request->num_wks_req}}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-3 control-label" for="consecutive-weeks">Consecutive Weeks:</div>
                    <div class="col-sm-9">
                        {{$request->consecutive_wks==1 ? 'Yes':'No'}}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <form class="form-horizontal" method="post" action="{{route('requests.store')}}">
                {{ csrf_field() }}
                <div class="col-md-3" style="height: 250px;">

                    <?php $month = ''; ?>
                    @foreach($season->weeks as $week)
                        @if($month != $week->start_date->format('F'))
                            <?php $month = $week->start_date->format('F') ?>
                            @if(!$loop->first)
                </div>
                <div class="col-md-3" style="height: 250px;">
                    @endif

                    <h3>{{$month}}</h3>


                    @endif
                    <div class="form-check">
                        <input class="form-check-input" name="weeks[{{$week->id}}]" type="checkbox"
                               value="{{$week->id}}"
                               id="Week-{{$week->id}}">
                        <label class="form-check-label" for="Week-{{$week->id}}">
                            {{$week->start_date->format('M, d')}} - {{$week->end_date->format('M, d')}}
                        </label>
                    </div>


                    @endforeach
                </div>
                <div class="form-group col-md-12">
                    <input type="hidden" readonly class="form-control-plaintext" name="season" id="season"
                           value="{{$season->id}}">
                    <input type="hidden" readonly class="form-control-plaintext" name="presenter" id="presenter"
                           value="{{$presenter->id}}">
                    <input type="hidden" readonly class="form-control-plaintext" name="number-weeks" id="number-weeks"
                           value="{{$request->num_wks_req}}">
                    <input type="hidden" readonly class="form-control-plaintext" name="consecutive-weeks"
                           id="consecutive-weeks"

                           {{--@if($request->consecutive_wks == 0)
                           value="NO"
                           @else
                           value="Yes"
                            @endif--}}
                           value="{{$request->consecutive_wks}}"
                    >
                    <!-- <button type="reset" role="button" class="col-sm-2 button btn btn-primary">Cancel</button> -->
                    <button type="submit" role="button" class="col-sm-2 button btn btn-primary pull-right">
                        Add
                    </button>
                </div>

        </div>
        </form>
    </div>

@endsection

@section('scripts')

@endsection