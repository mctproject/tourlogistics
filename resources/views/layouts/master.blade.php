<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title> @yield('title') </title>

    <!-- Bootstrap -->
    <link href='{{ mix('css/app.css')}}' rel="stylesheet" type="text/css"/>
    <link href='//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css' rel="stylesheet" type="text/css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php $topThreeSeasons = \App\Models\Season::topThree(); $seasons = \App\Models\Season::all()?>
    @yield('assets')
</head>
<body>
<script src="<?= url('/js/app.js');?>"></script>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <img style="padding: 0px 25px 0px 0px" src="{{ URL::to('/image/mct_logo(1).png') }}">
            <img style="padding-right: 25px" src="{{ URL::to('/image/tour_logistics_logo(1).png') }}">

        </div>
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                   aria-haspopup="true">
                    Presenters
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{route('presenters.index')}}">All Presenters</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href={{route('presenters.create')}}>Add Presenter</a></li>
                    <li><a href={{route('contacts.create')}}>Add Contact</a></li>
                </ul>
            </li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                   aria-haspopup="true">
                    Seasons
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    @foreach($topThreeSeasons as $seasonTab)
                        <li><a href={{route('season.show',[$seasonTab->id])}}>{{$seasonTab->name}}</a></li>
                    @endforeach
                    <li role="separator" class="divider"></li>
                    <li><a href="{{route('season.index')}}">Select Season</a></li>
                </ul>
            </li>


            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                   aria-haspopup="true">
                    Tour Teams
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{route('tourTeams.index')}}">All Teams</a></li>
                    <li role="separator" class="divider"></li>


                    @foreach($seasons as $season)
                            <li><a href={{route('tourTeams.show', [$season->id])}}>{{$season->name}}</a></li>
                        @endforeach
                    <li role="separator" class="divider"></li>
                    <li><a href={{route('tourTeams.create')}}>Add Team</a></li>
                    <li><a href={{route('shows.create')}}>Add Show</a></li>
                </ul>
            </li>



            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                   aria-haspopup="true">
                    Requests
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="{{route('requests.index')}}">All Requests</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="{{route('addRequest')}}">Add Request</a></li>
                </ul>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"
                   aria-haspopup="true">
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>


    </div>
</nav>

<main role="main">
    <div class="container">
        @yield('content')
    </div>
</main>


</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>


<script>
    /*$(document).ready(function () {
        $('#tours-data').DataTable({});
        // $('table').DataTable({});
        // $('table').DataTable({});
    });*/
</script>
@yield('scripts')
</body>
</html>