@extends('layouts.master')

@section('title')
    Create Presenter Page
@endsection

@section('content')


    <div class="container " id="collapseForm">
        <div class="row">
            <div class="col-lg-12">
                <form class="form-horizontal" method="post" action="{{route('presenters.store')}}">
                    {{ csrf_field() }}
                    <h2>Presenter Information</h2>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="presenter-name">Presenter Name:</label>
                        <div class="col-sm-9">
                            <input name="presenter-name" id="presenter-name" placeholder="Enter Presenter Name"
                                   class="input form-control">
                        </div>
                    </div>

                   <div class="form-group" style="visibility: hidden">
                        <label class="col-sm-3 control-label" for="fly-school">Fly School:</label>
                        <div class="col-sm-9">
                            <select name="fly-school" id="fly-school" class="form-control" aria-controls="help-pickup">
                                <option value="none">Is fly school?</option>
                                <option value='1'>
                                    True
                                </option>
                                <option selected="selected" value='0'>
                                    False
                                </option>
                            </select>
                            <p id="help-pickup" class="help-block" aria-live="polite"></p>
                        </div>
                    </div>

                    <h2>Primary Contact Information</h2>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="contact-name">Contact Name:</label>
                        <div class="col-sm-9">
                            <input type="text" name="contact-name" id="contact-name" placeholder="Contact Name"
                                   class="input form-control"
                                   data-format="1">
                        </div>
                    </div>

                    <div class=form-group>
                        <label class="col-sm-3 control-label" for="email">Email:</label>
                        <div class="col-sm-9">
                            <input type="text" name="email" id="email" placeholder="Enter Email"
                                   class="input form-control"
                                   data-format="1">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="address">Address:</label>
                        <div class="col-sm-9">
                            <input type="text" name="address" id="address" placeholder="Enter Address"
                                   class="input form-control"
                                   data-format="1">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="city">City:</label>
                        <div class="col-sm-9">
                            <input type="text" name="city" id="city" placeholder="Enter City"
                                   class="input form-control"
                                   data-format="1">
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="state">State:</label>
                        <div class="col-sm-9">
                            <select name="state" id="state" class="form-control" aria-controls="help-pickup">
                                <option value="none">Select State</option>
                                @foreach ($states as $state)
                                    <option value='{{$state->id}}'>
                                        {{$state->abbreviation}}
                                    </option>
                                @endforeach
                            </select>
                            <p id="help-pickup" class="help-block" aria-live="polite"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="zip-code">Zip Code:</label>
                        <div class="col-sm-9">
                            <input type="text" name="zip-code" id="zip-code" placeholder="Enter Zip Code"
                                   class="input form-control"
                                   data-format="1">
                        </div>
                    </div>

                    <div class=form-group>
                        <button type="reset" role="button" class="col-sm-2 button btn btn-primary">Cancel</button>
                        <button type="submit" role="button" class="col-sm-2 button btn btn-primary pull-right">Add
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--TESTING ADDING A NEW CONTACT TO AN EXISTING PRESENTER-->






@endsection

@section('scripts')

@endsection
