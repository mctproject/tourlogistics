@extends('layouts.master')

@section('title')
    Create Tour Team Page
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <form class="form" id="new-team" method="post" action="{{route('tourTeams.store')}}">
            {{ csrf_field() }}
            <div class="form-group mb-2">
                <label for="season">Season:</label>
                <select name="season" id="season" class="form-control">
                    <option value="none">Select Season</option>
                    @foreach ($seasons as $season)
                        <option value='{{$season->id}}'>
                            {{$season->name}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group mb-2">
                <label for="show">Show:</label>
                <select name="show" id="show" class="form-control">
                    <option value="none">Select Show</option>
                    @foreach ($shows as $show)
                        <option value='{{$show->abbreviation}}'>
                            {{$show->show_title}}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group col-md-12">
                <input type="hidden" readonly class="form-control-plaintext" name="complete" id="complete"
                       value = 0>
            </div>
            <button role="button" type="reset" class="btn btn-secondary">Cancel</button>
            <button role="button" type="submit" class="btn btn-primary">OK</button>
        </form>


        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection