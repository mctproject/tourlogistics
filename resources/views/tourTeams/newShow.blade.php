@extends('layouts.master')

@section('title')
    Create Show Page
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <form class="form" id="new-team" method="post" action="{{route('shows.store')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="abbreviation">Show Abbreviation:</label>
                        <div class="col-sm-9">
                            <input type="text" name="abbreviation" id="abbreviation"
                                   placeholder="Enter Show Abbreviation"
                                   class="input form-control">
                        </div>
                    </div>

                    <div class=form-group>
                        <label class="col-sm-3 control-label" for="show-title">Show Title:</label>
                        <div class="col-sm-9">
                            <input type="text" name="show-title" id="show-title" placeholder="Enter Show Title"
                                   class="input form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="reset" role="button" class="btn btn-secondary">Cancel</button>
                        <button type="submit" role="button" class="btn btn-primary">Add</button>

                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@section('scripts')

@endsection