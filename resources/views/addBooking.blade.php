@extends('layouts.master')

@section('title')
    Add Booking Page
@endsection
@section('assets')
<style>
    div.request-tile
    {
            width:400px;
            color:black;
            position: relative;
            border-radius: 5px;
            text-align: center;
    }
</style>
@endsection
@section('content')

<div class="container-fluid">
    <div class="row">
        <form class="form" id="new-booking" method="post" action="{{route('bookings.store')}}">
            {{ csrf_field() }}
            <div class="col-sm-3 control-label" class="form-group mb-2" style="width:100%">
                <h3>Reservation Request:</h3>
                {{-- <div  name="request" id="selectedRequest" >
                    Select Request                    
                </div> --}}
                <button class="btn btn-primary btn-lg" type="button" role="button" data-toggle="modal" data-target="#selectRequest" >Select Request</button>
                
            </div>
        <br>
        <br>
        <br>
        <br>
            <div class="form-group mb-2">
                <label class="col-sm-3 control-label" for="bookingWeek">Week:</label>
                <select name="bookingWeek" id="bookingWeek" class="form-control"
                        aria-controls="help-pickup">

                        <option value="" disabled="disabled" {{$weekId==null?'selected="selected"':""}}>Select week</option>

                        @foreach ($season->weeks as $week)
                            <option value='{{$week->id}}' {{$weekId==$week->id?'selected="selected"':""}}>
                                    {{\App\Models\Week::displayDates($week->start_date,$week->end_date)}}
                            </option>
                        @endforeach
                    </select>
            </div>
            <div class="form-group mb-2">
                    <label class="col-sm-3 control-label" for="bookingTourTeam">Show:</label>
                    <select name="bookingTourTeam" id="bookingTourTeam" class="form-control"
                            aria-controls="help-pickup">
                            <option value="" disabled="disabled" {{$tourTeamId==null?'selected="selected"':""}}>Select show</option>
                        @foreach ($season->tour_teams as $tourTeam)
                            <option value='{{$tourTeam->id}}' {{$tourTeamId==$tourTeam->id?'selected="selected"':""}}>
                                    {{\App\Models\Tour_team::display($tourTeam)}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-2">
                        <label class="col-sm-3 control-label" for="bookingStatus">Status:</label>
                        <select name="bookingStatus" id="bookingStatus" class="form-control"
                            aria-controls="help-pickup">
                                <option value="">Select status</option>
                            @foreach ($bookingStatuses as $bookingStatus)
                                <option value='{{$bookingStatus->id}}' title="{{$bookingStatus->description}}">
                                        {{$bookingStatus->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>

            <div class="form-group col-md-12">
                <input type="hidden" readonly class="form-control-plaintext" name="complete" id="complete"
                       value = 0>
            </div>

            <a href="{{route('season.show',$season->id)}}" type="button" class="btn btn-secondary" >Cancel</a>
            <button type="submit" class="btn btn-primary" onclick="return validInputs()">OK</button>
        </form>
    </div>
    <div id="selectRequest" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Select Request</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @if($requests!=null)    
                        @foreach($requests as $request)
                        <div style="float:right;top">
                            <button onclick="selectRequest(requestTileParent{{$request->id}})" type="reset" class="btn btn-primary" data-dismiss="modal" class="btn btn-default" >Select</button>
                        </div>
                        <div id="requestTileParent{{$request->id}}" style="padding:5px;">
                        <div id="requestTile{{$request->id}}" class="request-tile" style="background-color:#{{$request->presenter->primary_contact->first()->state->region->color}}">
                            <div style="font-size: 1.6em;font-weight: bold;">
                                {{$request->presenter->primary_contact->first()->city.', '.$request->presenter->primary_contact->first()->state->abbreviation}}
                            </div>
                            <div style="font-size: .7em;">
                                {{$request->presenter->presenter_name}}
                            </div>
                            <div>
                                {{-- {{dd($request->availableWeeks)}} --}}
                                @foreach($request->availableWeeks as $week)
                                @if($loop->index == 0)
                                {{\App\Models\Week::displayDates($week->start_date,$week->end_date)}}
                                @else
                                {{', '.\App\Models\Week::displayDates($week->start_date,$week->end_date)}}
                                @endif
                                @endforeach
                            </div>
                        <input type="hidden" class="form-control-plaintext" name="hfRequestId" id="hfRequestId" value="{{$request->id}}">
                        </div>
                        </div>
                        @endforeach
                        @else
                            <div>No Requests for given date.</div>
                        @endif
                        @if($otherRequests!=null)
                            <hr/>
                            <h4>Requests not for date and show.</h4>
                            <div>
                                @foreach($otherRequests as $otherRequest)
                                <div style="float:right;top">
                                    <button onclick="selectRequest(requestTileParent{{$otherRequest->id}})" type="reset" class="btn btn-primary" data-dismiss="modal" class="btn btn-default">Select</button>
                                </div>
                                <div id="requestTileParent{{$otherRequest->id}}" style="padding:5px;">
                                <div id="requestTile{{$otherRequest->id}}" class="request-tile" style="background-color:#{{$otherRequest->presenter->primary_contact->first()->state->region->color}}">
                                    <div style="font-size: 1.6em;font-weight: bold;">
                                        {{$otherRequest->presenter->primary_contact->first()->city.', '.$otherRequest->presenter->primary_contact->first()->state->abbreviation}}
                                    </div>
                                    <div style="font-size: .7em;">
                                        {{$otherRequest->presenter->presenter_name}}
                                    </div>
                                    <div>
                                        {{-- {{dd($otherRequest->availableWeeks)}} --}}
                                        @foreach($otherRequest->availableWeeks as $week)
                                        @if($loop->index == 0)
                                        {{\App\Models\Week::displayDates($week->start_date,$week->end_date)}}
                                        @else
                                        {{', '.\App\Models\Week::displayDates($week->start_date,$week->end_date)}}
                                        @endif
                                        @endforeach
                                    </div>
                                <input type="hidden" class="form-control-plaintext" name="hfRequestId" id="hfRequestId" value="{{$otherRequest->id}}">
                                </div>
                                </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection

@section('scripts')
<script>
    function selectRequest(tileName)
    {
      var secondDivContent = document.getElementById('selectedRequest');
      //secondDivContent.outerHTML = tileName.outerHTML;
      secondDivContent.innerHTML = tileName.innerHTML;
    }
    function validInputs()
    {
        if ($('#bookingWeek').val()==null)
        {
            alert("Please select a week.");
            return false;
        }
        if ($('#bookingTourTeam').val()==null)
        {
            alert("Please select a show.");
            return false;
        }
        if ($('#bookingStatus').val()==null)
        {
            alert("Please select a status.");
            return false;
        }
        if($('#selectedRequest').find('input[name="hfRequestId"]').val()==null)
        {
            alert("Please select a request");
            return false;
        }
        return true;
    }
</script>
@endsection