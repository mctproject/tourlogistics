@extends('layouts.master')

@section('title')
    Presenters Page
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <table class="table table-condensed table-striped table-bordered" id="tours-table" style="border-width: thick">
            <thead>
            <tr>

                <th>Presenter</th>
                <th>Contact Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>City</th>
                <th>Zip Code</th>
                <th>State</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($presenters as $presenter)
                <tr>
                    <td>{{$presenter->presenter_name}}</td>
                    <td>{{$presenter->primary_contact->first() ? $presenter->primary_contact->first()->contact_name : ""}}</td>
                    <td>{{$presenter->primary_contact->first() ? $presenter->primary_contact->first()->email : ""}}</td>
                    <td>{{$presenter->primary_contact->first() ? $presenter->primary_contact->first()->address : ""}}</td>
                    <td>{{$presenter->primary_contact->first() ? $presenter->primary_contact->first()->city : ""}}</td>
                    <td>{{$presenter->primary_contact->first() ? $presenter->primary_contact->first()->zip_code : ""}}</td>
                    <td>{{$presenter->primary_contact->first() ? $presenter->primary_contact->first()->state->abbreviation : ""}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('table').DataTable({});
        });
    </script>
@endsection
