@extends('layouts.master')

@section('title')
    Request Page
@endsection

@section('head')

@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <form class="form-inline" id="search-bar" role="search">
            <div class="input-group add-on">
                <input class="form-control" placeholder="Presenter Name" name="srch-term" id="srch-term" type="text">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form>
    </div>

    <div class="row">
        <table class="table table-condensed table-striped table-bordered" id="tours-table" style="border-width: thick">
            <thead>
            <tr>
                <th>Presenter</th>
                <th>Season</th>
                <th>Number Weeks Required</th>
                <th>Consecutive Weeks</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($requests as $request)
                <tr>
                    <td>{{$request->presenter->presenter_name}}</td>
                    <td>{{$request->season->name}}</td>
                    <td>{{$request->num_wks_req}}</td>
                    <td>{{$request->consecutive_wks ==1 ? 'Yes':'No'}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('table').DataTable({});
        });
    </script>
@endsection