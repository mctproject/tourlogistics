@extends('layouts.master')

@section('title')
    Season Page
@endsection

@section('content')
    <div class="row">
        {{--<form class="form-inline" id="season-form" method="post" action="">
            <div class="form-group mb-2">

                <label class="sr-only" for="season">Season Select:</label>
                <select name="season" id="season" class="form-control">
                    <option value="none">2017-18 School Year Season</option>
                </select>
            </div>
            <div class="form-group mb-2">
                <label for="week">Week:</label>

                <select name="week" id="week" class="form-control">
                    <option value="none">September 4-9</option>
                </select>
            </div>
            <div class="form-group mb-2">
                <label for="show"> Show:</label>

                <select name="show" id="show" class="form-control" aria-controls="help-pickup">
                    <option value="none">ALA-1</option>
                </select>
            </div>
            <button type="submit" role="button" class="col-sm-1 btn btn-primary pull-right">Options</button>
        </form>

        <form class="form-inline" id="search-bar" role="search">
            <div class="input-group add-on">
                <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                <div class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form>--}}

        {{--TODO: fix button--}}
        <button type="button" role="button" data-toggle="modal" data-target="#newSeasonPopup" class="col-sm-1 btn-lg btn-primary btn-circle pull-right glyphicon glyphicon-plus"></button>
    </div> 

    <div class="row">
        @foreach($seasons as $season)
            <div class="panel panel-default">
                <div class="panel-heading" style="overflow:hidden;">
                    <div class="btn-toolbar pull-left btn-group">
                      <a class="btn btn-primary btn-sm" href="{{route('season.show',$season->id)}}">Select</a>
                    </div>
                  <h3 class="panel-title" style="vertical-align:middle">{{$season->name}}</h3>
                </div>
                <div class="panel-body">
                    <table class="table borderless" style="border:none;">
                        <tr>
                            <td>
                                <label for="spnWeekCount">Number of Weeks</label>
                                <span id="spnWeekCount">{{$season->weeks->count()}}</span>
                            </td>
                            <td>
                                <label for="spnTourTeamCount">Number of Tour Teams</label>
                                <span>{{$season->tour_teams->count()}}</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        {{-- <li>
            <h3></h3>
        </li>
        <li role="separator" class="divider"></li>
        <li>
            
        </li> --}}
        @endforeach
    </div>
    <div class="modal fade" id="newSeasonPopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Add New Season</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form" id="modal-form" method="post" action="{{route('season.store')}}">
                        {{ csrf_field() }}
                        <div class="form-group mb-2">
                            <label class="col-sm-3 control-label" for="seasonType">Season Type</label>
                            <select name="seasonType" id="seasonType" class="form-control"
                                    aria-controls="help-pickup">
                                <option value="none">Select type</option>
                                @foreach ($seasonTypes as $seasonType)
                                    <option value='{{$seasonType->id}}'>
                                        {{$seasonType->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group mb-2">
                            <label class="col-sm-3 control-label" for="txbDescription">Description</label>
                            <input class="form-control" type="text" name="seasonDescription" id="seasonDescription">
                        </div>
                        <div class="form-group mb-2">
                            <label class="col-sm-3 control-label" for="seasonStartDate"></label>
                            <input class="form-control" type="date" name="seasonStartDate" id="seasonStartDate">
                        </div>
                        <div class="form-group mb-2">
                            <label class="col-sm-3 control-label" for="seasonEndDate"></label>
                            <input class="form-control" type="date" name="seasonEndDate" id="seasonEndDate">
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" role="button" class="btn btn-primary" >OK</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection