<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TourTeamsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_teams', function ($table) {

            //FK's
            $table->foreign('show_id')->references('abbreviation')->on('shows')->onDelete('cascade'); // FK from shows
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade'); // FK from seasons

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
