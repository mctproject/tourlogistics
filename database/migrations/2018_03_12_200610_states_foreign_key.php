<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatesForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('states', function ($table) {

            //FK's
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade'); // FK from countries
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade'); // FK from regions
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
