<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PresenterContactsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('presenter_contacts', function ($table) {

            // FK's
            $table->foreign('presenter_id')->references('id')->on('presenters')->onDelete('cascade'); // FK from presenters
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade'); // FK from contacts

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
