<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResRequestsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('res_requests', function ($table) {

            //FK's
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade'); // FK from season
            $table->foreign('presenter_id')->references('id')->on('presenters')->onDelete('cascade'); // FK from presenters

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
