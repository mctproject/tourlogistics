<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SeasonWeeksForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('season_weeks', function ($table) {

            // FK's
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade'); // FK from seasons
            $table->foreign('week_id')->references('id')->on('weeks')->onDelete('cascade'); // Fk from weeks

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
