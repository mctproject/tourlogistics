<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('states');

        Schema::create('states', function (Blueprint $table) {

            // Entity attributes
            $table->increments('id')->unique();
            $table->string('abbreviation');
            $table->string('description');
            
            $table->integer('region_id')->unsigned();
            $table->string('country_id',3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
