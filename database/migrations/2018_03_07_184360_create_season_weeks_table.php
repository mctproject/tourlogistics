<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('season_weeks');

        Schema::create('season_weeks', function (Blueprint $table) {

            // Entity attributes
            $table->increments('id'); // PK


            $table->integer('season_id')->unsigned();
            $table->integer('week_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('season_weeks');
    }
}
