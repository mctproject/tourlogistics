<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserPermissionsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_permissions', function ($table) {

            // FK's
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); // FK from users
            $table->foreign('permission_id')->references('id')->on('permissions')->onDelete('cascade'); // FK from permissions

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
