<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('res_requests');

        Schema::create('res_requests', function (Blueprint $table) {

            // Entity attributes
            $table->increments('id'); // PK
            $table->integer('num_wks_req');
            $table->boolean('consecutive_wks');
            $table->integer('season_id')->unsigned();
            $table->integer('presenter_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('res_requests');
    }
}
