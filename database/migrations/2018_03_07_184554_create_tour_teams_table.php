<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tour_teams');

        Schema::create('tour_teams', function (Blueprint $table) {

            // Entity attributes
            $table->increments('id'); // PK
            $table->integer('show_num');
            $table->boolean('complete');

            $table->string('show_id');
            $table->integer('season_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_teams');
    }
}
