<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTourTeamWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('tour_team_weeks');

        Schema::create('tour_team_weeks', function (Blueprint $table) {

            // Entity attributes
            $table->increments('id'); // PK


            $table->integer('tour_team_id')->unsigned();
            $table->integer('week_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_team_weeks');
    }
}
