<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('bookings');

        Schema::create('bookings', function (Blueprint $table) {

            // Entity attributes
            $table->increments('id');
            $table->string('routing_issues');
            $table->integer('week_id')->unsigned();
            $table->integer('presenter_id')->unsigned();
            $table->integer('tour_team_id')->unsigned();
            $table->integer('booking_status_id')->unsigned();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
