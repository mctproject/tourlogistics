<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AvailabilitiesForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //FK's
        Schema::table('availabilities', function ($table) {

            $table->foreign('week_id')->references('id')->on('weeks')->onDelete('cascade'); // FK for weeks
            $table->foreign('res_request_id')->references('id')->on('res_requests')->onDelete('cascade'); // FK for presenters

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
