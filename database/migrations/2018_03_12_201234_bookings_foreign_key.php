<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BookingsForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // FK's
        Schema::table('bookings', function ($table) {
            $table->foreign('week_id')->references('id')->on('weeks')->onDelete('cascade'); // FK from weeks
            $table->foreign('presenter_id')->references('id')->on('presenters')->onDelete('cascade'); // FK from presenters
            $table->foreign('tour_team_id')->references('id')->on('tour_teams')->onDelete('cascade'); // FK from tour_teams
            $table->foreign('booking_status_id')->references('id')->on('booking_statuses')->onDelete('cascade'); // FK from booking_statuses
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
