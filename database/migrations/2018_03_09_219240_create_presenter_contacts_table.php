<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresenterContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('presenter_contacts');

        Schema::create('presenter_contacts', function (Blueprint $table) {

            // Entity attributes
            $table->increments('id'); // PK

            $table->integer('presenter_id')->unsigned();
            $table->integer('contact_id')->unsigned();
            $table->integer('contact_type_id')->default(10);

            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presenter_contacts');
    }
}
