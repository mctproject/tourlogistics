<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('contacts');

        Schema::create('contacts', function (Blueprint $table) {

            // Entity attributes
            $table->increments('id'); // PK

            $table->string('contact_name');
            $table->string('email');
            $table->string('address');
            $table->string('city');
            $table->string('zip_code');

            $table->integer('state_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
