<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TourTeamWeeksForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour_team_weeks', function ($table) {

            //FK's
            $table->foreign('tour_team_id')->references('id')->on('tour_teams')->onDelete('cascade'); // FK from tour_teams
            $table->foreign('week_id')->references('id')->on('weeks')->onDelete('cascade'); // FK from weeks

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
