<?php

use Illuminate\Database\Seeder;



class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Contact types
        $contactTypes = json_decode(file_get_contents('database/data/contact_types.json'));
        
    	foreach($contactTypes as $contactType){

    		$newContactType= new App\Models\Contact_type();

    		$newContactType->id = $contactType->id;
    		$newContactType->type_name = $contactType->type_name;
    		$newContactType->save();
        }
        
        //season types
        $seasonTypes = json_decode(file_get_contents('database/data/season_types.json'));
        
    	foreach($seasonTypes as $seasonType){

    		$newSeasonType= new App\Models\Season_type();

    		$newSeasonType->id = $seasonType->id;
    		$newSeasonType->name = $seasonType->name;
    		$newSeasonType->save();
        }

        //regions
        $regions = json_decode(file_get_contents('database/data/regions.json'));
        
    	foreach($regions as $region){

    		$newRegion= new App\Models\Region();

    		$newRegion->id = $region->id;
    		$newRegion->name = $region->name;
			$newRegion->color = $region->color;
    		$newRegion->save();
        }

        $permissions = json_decode(file_get_contents('database/data/permissions.json'));

        foreach($permissions as $permission){
            $new_permission = new App\Models\Permission();

            $new_permission->id = $permission->Permission_id;
            $new_permission->name = $permission->Permission;
            $new_permission->description = $permission->Description;
            $new_permission->save();
        }

        //booking statuses
        $bookingStatuses = json_decode(file_get_contents('database/data/booking_statuses.json'));
        
    	foreach($bookingStatuses as $bookingStatus){

    		$newBookingStatus= new App\Models\Booking_status();

    		$newBookingStatus->id = $bookingStatus->id;
    		$newBookingStatus->name = $bookingStatus->name;
			$newBookingStatus->description = $bookingStatus->description;
    		$newBookingStatus->save();
        }

        //contries
        $countries = json_decode(file_get_contents('database/data/countries.json'));
        
    	foreach($countries as $country){

    		$newCountry= new App\Models\Country();

    		$newCountry->id = $country->id;
    		$newCountry->name = $country->name;
    		$newCountry->save();
        }

        //states
        $states = json_decode(file_get_contents('database/data/states.json'));
        
    	foreach($states as $state){

    		$newState= new App\Models\State();

    		$newState->id = $state->id;
    		$newState->abbreviation = $state->abbreviation;
    		$newState->description = $state->description;
    		$newState->region_id = $state->region_id;
    		$newState->country_id = $state->country_id;
    		$newState->save();
        }

        //shows
        $shows = json_decode(file_get_contents('database/data/shows.json'));
        
    	foreach($shows as $show){

    		$newShow= new App\Models\Show();

    		$newShow->abbreviation = $show->abbreviation;
    		$newShow->show_title = $show->show_title;
    		$newShow->save();
        }


        //presenters
        $presenters = json_decode(file_get_contents('database/data/presenters.json'));
        
    	foreach($presenters as $presenter){

    		$newPresenter= new App\Models\Presenter();

    		$newPresenter->id = $presenter->id;
            $newPresenter->presenter_name = $presenter->presenter_name;
            $newPresenter->is_fly_school = false;
    		$newPresenter->save();
        }

        //contacts
        $contacts = json_decode(file_get_contents('database/data/contacts.json'));

        foreach($contacts as $contact){

            $newContact= new App\Models\Contact();

            $newContact->id = $contact->id;
            $newContact->contact_name = $contact->contact_name;
            $newContact->email = $contact->email;
            $newContact->address = $contact->address;
            $newContact->city = $contact->city;
            $newContact->zip_code = $contact->zip_code;
            $newContact->state_id = $contact->state_id;
            //$newContact->contact_type_id = $contact->contact_type_id;
            $newContact->save();
            if(isset($contact->presenters)) {
                foreach($contact->presenters as $presenter){
                    $newContact->presenters()->save(\App\Models\Presenter::find($presenter->id), ['contact_type_id' => $presenter->contact_type_id]);
                }
            }
        }


        //weeks
        $weeks = json_decode(file_get_contents('database/data/weeks.json'));
        
    	foreach($weeks as $week){

    		$newWeek= new App\Models\Week();

    		$newWeek->id = $week->id;
            $newWeek->start_date = new DateTime($week->start_date);
            $newWeek->end_date = new DateTime($week->end_date);
    		$newWeek->save();
        }

        //season 
        $seasons = json_decode(file_get_contents('database/data/seasons.json'));
        
    	foreach($seasons as $season){

    		$newSeason= new App\Models\Season();

    		$newSeason->id = $season->id;
            $newSeason->complete = $season->complete;
            $newSeason->season_type_id = $season->season_type_id;
            $newSeason->name = $season->name;
            $newSeason->save();

            if(isset($season->weeks))
            {
                foreach($season->weeks as $seasonWeek)
                {
                    $newSeason->weeks()->save(\App\Models\Week::find($seasonWeek->week_id));
                }
            }
        }



        //Tour teams
        $tourTeams = json_decode(file_get_contents('database/data/tour_teams.json'));

        foreach($tourTeams as $tourTeam){

            $newTourTeam= new App\Models\Tour_team();

            $newTourTeam->id = $tourTeam->id;
            $newTourTeam->show_num = $tourTeam->show_num;
            $newTourTeam->complete = $tourTeam->complete;
            $newTourTeam->show_id = $tourTeam->show_id;
            $newTourTeam->season_id = $tourTeam->season_id;
            $newTourTeam->save();
        }

        //Tour team weeks
        $ttweeks = json_decode(file_get_contents('database/data/tour_team_weeks.json'));

        foreach($ttweeks as $week){

            $newttWeek= new App\Models\Tour_team_week();

            $newttWeek->id = $week->id;
            $newttWeek->tour_team_id = $week->tour_team_id;
            $newttWeek->week_id = $week->week_id;
            $newttWeek->save();
        }
    }
}
